'use strict';

/* Controllers */

var stack = null;

var stackControllers = angular.module('stackControllers', []);

stackControllers.controller('StackCtrl', function($scope, $location, $sce, hotkeys) {
    $scope.selected = 0;

    var localStorage = window.localStorage;
    
    if(stack == null) {
        if(!localStorage.stack) {
            localStorage.stack = JSON.stringify([]); // Init the very first time
        }
        stack = JSON.parse(localStorage.stack); // Load model
    }

    var stackView = [];
    for(var i in stack) {
        var item = stack[i];
        var itemView = {};
        itemView.name = item.name;
        itemView.desc = micromarkdown.parse(item.desc);
        stackView.push(itemView);
    }

    $scope.stack = stackView;

    hotkeys.add({
        combo: 'w',
        description: 'Up',
        callback: function() {
            if($scope.selected > 0) {
                $scope.selected--;
            }
        }
    });
    hotkeys.add({
        combo: 's',
        description: 'Down',
        callback: function() {
            if($scope.selected < $scope.stack.length-1) {
                $scope.selected++;
            }
        }
    });
    hotkeys.add({
        combo: 'q',
        description: 'cUp',
        callback: function() {
            if($scope.selected > 0) {
                var curItem = $scope.stack[$scope.selected];
                $scope.stack[$scope.selected] = $scope.stack[$scope.selected-1];
                $scope.stack[$scope.selected-1] = curItem;
                $scope.selected--;
                saveStack();
            }
        }
    });
    hotkeys.add({
        combo: 'a',
        description: 'cDown',
        callback: function() {
            if($scope.selected < $scope.stack.length-1) {
                var curItem = $scope.stack[$scope.selected];
                $scope.stack[$scope.selected] = $scope.stack[$scope.selected+1];
                $scope.stack[$scope.selected+1] = curItem;
                $scope.selected++;
                saveStack();
            }
        }
    });
    hotkeys.add({
        combo: 'd',
        description: 'Done',
        callback: function() {
            if($scope.stack.length > 0) {
                if(confirm('Are you sure you are done with the selected task?')) {
                    $scope.selected = 0;
                    $scope.stack.shift();
                    saveStack();
                }
            }
        }
    });
    hotkeys.add({
        combo: 'c',
        description: 'Create',
        callback: function() {
            $location.path('/new-item');
        }
    });
    /*hotkeys.add({ // clear previous func
        combo: 's',
        description: 'Submit',
        callback: function() {
        }
    });*/
    //TODO Desc of item
});

var submitFunc = function($scope, $location) {
    if($scope.newItem.name.trim() === '') {
        alert('Error: The Name field is empty!');
    } else {
        var item = $scope.newItem;
        // TODO: escape item?
        stack.unshift(item);
        saveStack();
        $location.path('/stack');
    }
}

stackControllers.controller('NewItemCtrl', ['$scope', '$location', 'hotkeys',
  function($scope, $location, hotkeys) {

    $scope.newItem = {
        name: '',
        desc: ''
    };

    $scope.submit = function() {
        submitFunc($scope, $location);
    };

    hotkeys.add({
        combo: 's',
        description: 'Submit',
        callback: $scope.submit
    });
    hotkeys.add({
        combo: 'c',
        description: 'Cancel',
        callback: function() {
            $location.path('/stack');
        }
    });
}]);

function saveStack() {
    localStorage.stack = JSON.stringify(stack);
}