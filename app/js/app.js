
var todoApp = angular.module('todoApp', ['cfp.hotkeys', 'ngRoute', 'ngSanitize', 'stackControllers']);

todoApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/stack', {
        templateUrl: 'parts/stack.html',
        controller: 'StackCtrl'
      }).
      when('/new-item', {
        templateUrl: 'parts/new-item.html',
        controller: 'NewItemCtrl'
      }).
      otherwise({
        redirectTo: '/stack'
      });
  }]);
